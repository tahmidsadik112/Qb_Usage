(function () {
    var UsageByDate = new function (dates, usage) {
//    this.dates = dates;
//    this.usage = usage;

        var data = {
            dates: [],
            usage: []

        };

        //this sanitizer takes specific dom element and returs an UsageByDate object
        //Have to check for the right page here

        //grab all the table elements
        var tables = document.getElementsByTagName("table");

        // containerTable is the parent table which contains usageTable
        var containerTable = tables[2];

        // This contains the usage data we are interested in
        var usageTable = tables[3];

        var raw_data = usageTable.getElementsByTagName("b");

        // Create a new svg element and insert it after containerTable
        var svg = document.createElement("svg");
        svg.setAttribute("class", "chart");
        containerTable.parentNode.insertBefore(svg, containerTable.nextSibling);


        data.dates.length = 0;
        data.usage.length = 0;

        for (var i = 2; i < raw_data.length; i++) {
            if (i % 2 == 0) {
                data.dates.push(raw_data[i].textContent);
            }
            else {
                data.usage.push(raw_data[i].textContent);
            }
        }


    }
})();
